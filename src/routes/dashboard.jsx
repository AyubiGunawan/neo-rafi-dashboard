// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import ContentPaste from "@material-ui/icons/ContentPaste";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import Photo from "@material-ui/icons/Photo";
//import Notifications from "@material-ui/icons/Notifications";
//import Unarchive from "@material-ui/icons/Unarchive";
// core components/views
//import DashboardPage from "views/Dashboard/Dashboard.jsx";
//import UserProfile from "views/UserProfile/UserProfile.jsx";
//import TableList from "views/TableList/TableList.jsx";
//import Typography from "views/Typography/Typography.jsx";
//import Icons from "views/Icons/Icons.jsx";
//import Maps from "views/Maps/Maps.jsx";
//import NotificationsPage from "views/Notifications/Notifications.jsx";
//import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.jsx";
import views from 'views';

import { ROUTES } from 'configs'

let { NewsListPage, UserListPage, RewardListPage, RedeemListPage, FrameListPage, PromoListPage } = views;

const dashboardRoutes = [
  {
    path: ROUTES.DASHBOARD(),
    sidebarName: "News",
    navbarName: "News List",
    icon: Dashboard,
    component: NewsListPage
  },
  {
    path: ROUTES.LIST_USER(),
    sidebarName: "Users List",
    navbarName: "Users List",
    icon: Person,
    component: UserListPage
  },
  {
    path: ROUTES.LIST_REWARD(),
    sidebarName: "Reward List",
    navbarName: "Reward List",
    icon: ContentPaste,
    component: RewardListPage
  },
  {
    path: ROUTES.LIST_REDEEM(),
    sidebarName: "Redeem List",
    navbarName: "Redeem List",
    icon: LibraryBooks,
    component: RedeemListPage
  },
  {
    path: ROUTES.LIST_PROMO(),
    sidebarName: "Promo List",
    navbarName: "Promo List",
    icon: BubbleChart,
    component: PromoListPage
  },
  {
    path: ROUTES.LIST_FRAME(),
    sidebarName: "Frame List",
    navbarName: "Frame List",
    icon: Photo,
    component: FrameListPage
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
