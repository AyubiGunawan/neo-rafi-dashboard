import TableList from "views/TableList/TableList.jsx";
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import NotificationsPage from "views/Notifications/Notifications.jsx";

import views from 'views';

import { ROUTES } from 'configs'

let { 
  NewsListPage, 
  NewsDetailPage,
  UserListPage, 
  RewardListPage, 
  RedeemListPage, 
  FrameListPage, 
  PromoListPage, 
  PromoDetailPage, 
  UserDetailPage, 
  } = views;

const routes = [
  {
    path: ROUTES.DASHBOARD(),
    navbarName: "News List",
    component: NewsListPage
  },
  {
    path: ROUTES.DETAIL_NEWS(':id'),
    navbarName: "News Detail",
    component: NewsDetailPage
  },
  {
    path: ROUTES.CREATE_NEWS(),
    navbarName: "Create News",
    component: Typography
  },
  {
    path: ROUTES.EDIT_NEWS(':id'),
    navbarName: "Edit News",
    component: Icons
  },
  {
    path: ROUTES.LIST_PROMO(),
    navbarName: "Promo List",
    component: PromoListPage
  },
  {
    path: ROUTES.DETAIL_PROMO(':id'),
    navbarName: "Promo Detail",
    component: PromoDetailPage
  },
  {
    path: ROUTES.CREATE_PROMO(),
    navbarName: "Create Promo",
    component: Typography
  },
  {
    path: ROUTES.EDIT_PROMO(':id'),
    navbarName: "Edit Promo",
    component: Icons
  },
  {
    path: ROUTES.LIST_USER(),
    navbarName: "Users List",
    component: UserListPage
  },
  {
    path: ROUTES.DETAIL_USER(':id'),
    navbarName: "User Detail",
    component: UserDetailPage
  },
  {
    path: ROUTES.LIST_REWARD(),
    navbarName: "Rewards List",
    component: RewardListPage
  },
  {
    path: ROUTES.REWARD_BY_USER(':id'),
    navbarName: "Reward Detail by User",
    component: TableList
  },
  {
    path: ROUTES.REVIEW_BY_USER(':id'),
    navbarName: "Review Detail by User",
    component: TableList
  },
  {
    path: ROUTES.LIST_REDEEM(),
    navbarName: "Redeems List",
    component: RedeemListPage
  },
  {
    path: ROUTES.LIST_FRAME(),
    navbarName: "Frames List",
    component: FrameListPage
  },
  {
    path: ROUTES.CREATE_FRAME(),
    navbarName: "Create Frame",
    component: Typography
  },
  {
    path: ROUTES.UPDATE_FRAME(':id'),
    navbarName: "Edit Frame",
    component: Icons
  },
  {
    path: ROUTES.CONFIG_REWARD(),
    navbarName: "Rewards Config",
    component: NotificationsPage
  },
  {
    path: ROUTES.CONFIG_APP(),
    navbarName: "Application Config",
    component: NotificationsPage
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default routes;
