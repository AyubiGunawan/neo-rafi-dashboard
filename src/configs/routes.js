const routes = {
  HOME() { return `/`; },
  LOGIN() { return `/login`; },
  
  DASHBOARD() { return `/dashboard`; },
  LIST_NEWS() { return `/news`; },
  DETAIL_NEWS(id) { return `/detailnews/${id}`; },
  CREATE_NEWS() { return `/news/add`; },
  EDIT_NEWS(id) { return `/editnews/${id}`; },
  
  LIST_PROMO() { return `/promo`; },
  DETAIL_PROMO(id) { return `/detailpromo/${id}`; },
  CREATE_PROMO() { return `/promo/add`; },
  EDIT_PROMO(id) { return `/editpromo/${id}`; },
  
  LIST_USER() { return `/user`; },
  DETAIL_USER(id) { return `/detailuser/${id}`; },
  
  LIST_REWARD() { return `/reward`; },
  LIST_REDEEM() { return `/redeem`; },
  REWARD_BY_USER(id) { return `/user/${id}/reward`; },
  REVIEW_BY_USER(id) { return `/user/${id}/review`; },
  
  LIST_FRAME() { return `/frame`; },
  CREATE_FRAME() { return `/frame/add`; },
  UPDATE_FRAME(id) { return `/frame/edit/${id}`; },
  
  CONFIG_REWARD() { return `/reward`; },
  CONFIG_APP() { return `/config`; }
};

export default routes;