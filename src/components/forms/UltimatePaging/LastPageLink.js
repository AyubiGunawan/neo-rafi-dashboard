import React from 'react';
import PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";

import paginationStyle from "assets/jss/material-dashboard-react/components/paginationStyle.jsx";

const LastPageLink = ({ classes, onClick, isDisabled }) => {
  const paginationLink = classNames({
    [classes.paginationLink]: true,
  });
  return (
    <Button onClick={onClick} className={paginationLink} disabled={isDisabled}>
      {'>>'}
    </Button>
  );
}
LastPageLink.propTypes = {
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
};

export default withStyles(paginationStyle)(LastPageLink);