import React from 'react';
import PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";

import paginationStyle from "assets/jss/material-dashboard-react/components/paginationStyle.jsx";

const Page = ({ classes, value, isActive, onClick, isDisabled }) => {
  const paginationLink = classNames({
    [classes.paginationLink]: true,
    [classes['info']]: isActive,
    [classes.disabled]: isDisabled
  });
  return(
    <Button onClick={onClick} className={paginationLink} primary={isActive.toString()} disabled={isDisabled}>
      {value.toString()}
    </Button>
  );
}
Page.propTypes = {
  classes: PropTypes.object.isRequired,
  value: PropTypes.number,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
};

export default withStyles(paginationStyle)(Page);