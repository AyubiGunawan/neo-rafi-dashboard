import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchNewsList(val) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_NEWS_SECTION_LIST + '/' + val,
      header: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listOfNewsFetchedAction(res));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOfNewsFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchNewsDelete(data) {
  return dispatch => {
    const options = {
      method: 'delete',
      url: SERVICES.DELETE_NEWS + '/' + data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(deleteOfNewsFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/news';
      })
      .catch(() => {
        dispatch(deleteOfNewsFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_ASIAN_GAMES_NEWS_FETCHED,
    data
  };
}

function deleteOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.DELETE_NEWS,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}