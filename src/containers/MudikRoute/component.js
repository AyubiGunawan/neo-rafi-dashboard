import React from 'react';
import PropTypes from "prop-types";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      open: false,
      id: null,
      title: '',
      value: 0,
      mark: 'JALURMUDIK'
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleDeleteNews = this._handleDeleteNews.bind(this);
  }

  componentWillMount() {
    let { actions } = this.props;
    let { mark } = this.state;

    actions.fetchNewsList(mark);
  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_MUDIK_ROUTE_FETCHED } = ACTIONS;
    let { type } = nextProps;
    if (type === LIST_OF_MUDIK_ROUTE_FETCHED) {
      this.setState({render:true});
    }
  }

  _handleChange = (event, value) => {
    let { actions } = this.props;
    let { mark } = this.state;

    actions.fetchNewsList(mark);
  };

  _handleDeleteNews = () => {
    const { actions } = this.props;
    //    actions.fetchDeleteUser(userId);
    actions.fetchNewsDelete(this.state.id);
    this.setState({ open: false, id:null, title:'' });
  }

  _openModal(id, title) {
    this.setState({ open: true, id, title });
  }

  _closeModal() {
    this.setState({ open: false, id:null, title:'' });
  }

  render() {
    let { classes, isLoading, data } = this.props;
    return (
      <section>
        {isLoading ? 
            <h6>loading...</h6> 
          : 
            <GridList cellHeight={160} className={classes.gridList} cols={3}>
              {data.map((tile, index) => (
                <GridListTile key={index} cols={tile.cols || 1}>
                  <img src={tile.newsImage} alt={tile.newsTitle} />
                  <GridListTileBar
                    title={tile.newsTitle}
                    subtitle={<span>Loc: {tile.newsLocation}</span>}
                    actionIcon={
                      <IconButton className={classes.icon}>
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </GridListTile>
              ))}
            </GridList>    
        }
      </section>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.array,
  isLoading: PropTypes.bool
};