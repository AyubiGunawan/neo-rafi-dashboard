import React from "react";
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import Slide from "@material-ui/core/Slide";
import Close from "@material-ui/icons/Close";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import moment from 'moment';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";

import { ACTIONS } from 'constants/index';

function Transition(props) {
  return <Slide direction = "down" { ...props} />;
}

export default class Component extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      render:false,
      post: null
    }
  }

  componentDidMount() {
    const { actions, match } = this.props;
    actions.fenchPostListByUser(match.params.id);

  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_POST_BY_USER_FETCHED } = ACTIONS;
    let { type } = nextProps;

    if (type === LIST_OF_POST_BY_USER_FETCHED) {
      this.setState({render: true})
    }
  }

  handleDeletePost = (data) => {
    const { actions } = this.props;
    actions.fenchDeletePostById(data);
  }

  _openModal(item) {
    this.setState({ open: true, post:item });
  }

  _closeModal() {
    this.setState({
      open: false,
      id: null,
      rd: null,
      title: ''
    });
  }

  _renderModal() {
    const { classes } = this.props;
    if (this.state.post !== null) {
      let { title, createdDate, photo, caption, category } = this.state.post;
    return(
        <Dialog
          classes={{
            root: classes.center,
            paper: classes.modal
          }}
          open={this.state.open}
          onClose={this._closeModal}
          TransitionComponent={Transition}
          keepMounted
          aria-labelledby="alert-slide-title"
          aria-describedby="alert-slide-description"
        >
          <DialogTitle 
            id = "classic-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <IconButton
              className={classes.modalCloseButton}
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={() => this._closeModal()}>
              <Close className={classes.modalClose} />
            </IconButton>
          </DialogTitle>

          <DialogContent
            id="modal-slide-description"
            className={classes.modalBody}
          >
            <img src={photo} alt='aww' width="100%" />
            <div className={classes.space}>
              <h6>{title}</h6>
              <div>{moment(createdDate).format('DD-MM-YYYY')}</div>
              <p>
                {caption}
              </p>
            {Array.isArray(category) ? category.join(', ') : category}
            </div>
          </DialogContent>
        </Dialog>
      );
    } else {
     return (
        <Dialog
          classes={{
            root: classes.center,
            paper: classes.modal
          }}
          open={this.state.open}
          onClose={this._closeModal}
          TransitionComponent={Transition}
          keepMounted
          aria-labelledby="alert-slide-title"
          aria-describedby="alert-slide-description"
        >
          <DialogTitle 
            id = "classic-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <IconButton
              className={classes.modalCloseButton}
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={() => this._closeModal()}>
              <Close className={classes.modalClose} />
            </IconButton>
          </DialogTitle>

          <DialogContent
            id="modal-slide-description"
            className={classes.modalBody}
          >
          kosong
          </DialogContent>
        </Dialog>
      );
    }
  
  }

  _renderDetail() {
    let { classes, data } = this.props;

    if(!this.state.render) {
      return (
        <h1>TIDAK ADA DATA</h1>
      );
    } else {
      return (
        <Grid container>
          <GridItem xs={12} sm={12} md={4}>
            <Card profile>
              <CardAvatar profile>
                <a href="#pablo" onClick={e => e.preventDefault()}>
                  <img src={data.data.photoProfile} alt="..." />
                </a>
              </CardAvatar>
              <CardBody profile>
                <h6 className={classes.cardCategory}>{data.data.role}</h6>
                <h4 className={classes.cardTitle}>{data.data.name}</h4>
                <p className={classes.description}>
                  {data.data.personalQuote}
                </p>
              </CardBody>
            </Card>
          </GridItem>
          {data.data.post.length > 0 ? this._renderTable() : ''}
          {this._renderModal()}
        </Grid>
      );
    }
  }

  _renderTable() {
    let { classes, data, match } = this.props;

    return (
      <GridItem xs={12} sm={12} md={8}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Post List</h4>
          </CardHeader>
          <CardBody>
            <GridList cellHeight={160} className={classes.gridList} cols={3}>
              {data.data.post.map((tile, index) => (
                <GridListTile key={index} cols={tile.cols || 1}>
                  <img src={tile.photo} alt={tile.newsTitle} />
                  <GridListTileBar
                    title={tile.caption}
                    subtitle={<span>{Array.isArray(tile.category) ? tile.category.join(', ') : tile.category}</span>}
                    actionIcon={
                      <div>
                        <IconButton
                          aria-label="Show"
                          className={classes.tableActionButton}
                          onClick={() => this._openModal(tile)}
                        >
                          <InfoIcon
                            className={
                              classes.tableActionButtonIcon + " " + classes.edit
                            }
                          />
                        </IconButton>
                        <IconButton
                          onClick={() => this.handleDeletePost({ userId: match.params.id, postId: tile.id })}
                          aria-label="Close"
                          className={classes.tableActionButton}
                        >
                          <Close
                            className={
                              classes.tableActionButtonIcon + " " + classes.close
                            }
                          />
                        </IconButton>
                      </div>
                    }
                  />
                </GridListTile>
              ))}
            </GridList>    

          </CardBody>
          </Card>
        </GridItem>
    );
  }
  _renderLoading() {
    return (
      <Grid container>
        <h6>loading...</h6>
      </Grid>
    );
  }

  render() {
    const { isLoading } = this.props;

    return (
        isLoading ? 
          this._renderLoading()
        : 
          this._renderDetail()
        
    );
  }

}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};