import React from "react";
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import IconButton from "@material-ui/core/IconButton";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Close from "@material-ui/icons/Close";
import Business from "@material-ui/icons/Business";
//import { ROUTES } from 'configs';
import UltimatePaging from 'components/forms/UltimatePaging';
import Tooltip from "@material-ui/core/Tooltip";
import { ACTIONS } from 'constants/index';
import {CSVLink} from 'react-csv';
import Modal from 'components/Modal/Modal.jsx';

const json2csv = require('json2csv').parse;

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      name: '',
      id: null,
      render: false
    }
    this._closeModal = this._closeModal.bind(this);
    this._handleDeleteReward = this._handleDeleteReward.bind(this);
  }

  componentDidMount() {
    const { actions } = this.props;

    actions.fetchRewardList();

  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_REWARD_FETCHED } = ACTIONS;
    let { type } = nextProps;

    if( type ===  LIST_OF_REWARD_FETCHED ) {
      this.setState({render: true});
    }
  }

  onPageChange(page) {
    let { actions } = this.props;
    let { meta } = this.props.data.data;

    this.setState({ page });
    actions.fetchUserList({ page, count: meta.size });
  }

  _handleDeleteReward = () => {
    let { actions } = this.props;
    actions.fetchDeleteUser(this.state.id);

    this.setState({
      open: false,
      name: '',
      id: null
    });
  }

  _openModal(name, id) {
    this.setState({
      open: true,
      name,
      id
    });
  }

  _closeModal() {
    this.setState({
      open: false,
      name: '',
      id: null
    });
  }

  handleCsvReward = () => {
    let { data } = this.props.data;
    const csv = json2csv(data);
    console.log(csv);
  }

  _renderModal() {
    let { classes } = this.props;
    return (
      <Modal open={this.state.open} closeModal={this._closeModal} onClick={this._handleDeleteReward}>
        <div>
          <h6 className={classes.modalTitle}>
            {
              'Delete:' + this.state.name
            }
          </h6>

        </div>
      </Modal>
    );
  }

  _renderTable() {
    const { data } = this.props;

    if(!this.state.render) {
      return (
        <h1>TIDAK ADA DATA</h1>
      );
    } else {
      return (
        <Table
          tableHeaderColor="primary"
          tableHead={["No", "Nama", "Active Status", "Max Redeem", "Quantity", "Value", "Actions"]}
        >
          <TableBody>
          {data.data &&
            (
              data.data.map((item, index) => this._renderTableRow(item, index + 1))
            )
          }
          </TableBody>
          {this._renderTableMeta()}
        </Table>
      );
    }
  }

  _renderTableRow(item, index) {
    const { _id, name, is_Active, max_redeem, quantity, value } = item;
    const { classes } = this.props;

    return (
      <TableRow key={_id}>
        <TableCell className={classes.tableCell}>{index}</TableCell>
        <TableCell className={classes.tableCell}>{name}</TableCell>
        <TableCell className={classes.tableCell}>{is_Active ? 'active' : 'inactive'}</TableCell>
        <TableCell className={classes.tableCell}>{max_redeem}</TableCell>
        <TableCell className={classes.tableCell}>{quantity}</TableCell>
        <TableCell className={classes.tableCell}>{value}</TableCell>
        <TableCell className={classes.tableCell}>
          <Tooltip
            id="tooltip-top"
            title="Reward Delete"
            placement="top"
            classes={{ tooltip: classes.tooltip }}
          >
            <IconButton
              onClick={() => this._openModal(name, _id)}
              aria-label="Close"
              className={classes.tableActionButton}
            >
              <Close
                className={
                  classes.tableActionButtonIcon + " " + classes.close
                }
              />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
    );
  }

  _renderTableMeta() {
    const { data, page } = this.props;

    if (data.data && data.data.meta) {
      return (
        <TableFooter>
          <TableRow>
            <TableCell colSpan={4}>
              <UltimatePaging
                totalPages={(data.data.meta.totalPages !== undefined) 
                  ? data.data.meta.totalPages : 1}
                currentPage={page}
                onChange={this.onPageChange}
                />
            </TableCell>
          </TableRow>
        </TableFooter>
      );
    } else {
      return (<TableFooter />);
    }
  }

  render() {
    const { isLoading, classes, data } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <Grid container>
                <GridItem xs={6} sm={6} md={6}>
                  <h4 className={classes.cardTitleWhite}>Reward</h4>
                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  <div className={classes.side}>
                    {this.state.render ?
                      <CSVLink data={ json2csv(data.data, ['_id'])} filename={"rewardlist.csv"}>
                        <Business
                          className={
                            classes.tableActionButtonIcon + " " + classes.download
                          }
                        />
                      </CSVLink>
                    :
                      ' '
                    }
                  </div>
                </GridItem>
              </Grid>
            </CardHeader>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable()}
            </CardBody>
          </Card>
        </GridItem>
        {this._renderModal()}
      </Grid>
    );
  }

}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};