import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import Component from './component';
import signupPageStyle from "assets/jss/material-dashboard-react/views/signupPageStyle.jsx";


function mapStateToProps(state) {
  const { data } = state.login;
  const { isLoading } = state.loading;

  return {
    isLoading,
    data,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

const Styled = withStyles(signupPageStyle)(Component);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);
