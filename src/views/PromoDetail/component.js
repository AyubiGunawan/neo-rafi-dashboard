import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { ACTIONS } from 'constants/index';
import Quote from "components/Typography/Quote.jsx";

export default class Component extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      render: false
    }
  }

  componentDidMount() {
    let { actions, match } = this.props;
    actions.fenchGetDetailPromo(match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    let { DETAIL_OF_PROMO_FETCHED } = ACTIONS;
    let { type, data } = nextProps;

    if (type === DETAIL_OF_PROMO_FETCHED) {
      this.setState({render: true});
      console.log(data)
    }
  }

  _renderTable() {

    if (!this.state.render) {
      return ( <h1> TIDAK ADA DATA </h1>
      );
    } else {
      let { classes, data } = this.props
      let { listNewsPromoImage, listNewsCategory, listNewsPromoDescription, liststartDate, listoutDate, listNewsPromoName } = data;
      return (
        <div>
          <div>
            <h2>{listNewsPromoName}</h2>
            <img src={listNewsPromoImage[0]} alt='aww' width="100%" />
          </div>
          <div className={classes.typo}>
            <div className={classes.note}>
              <Quote
                text={listNewsCategory + '.'}
                author={' ' + liststartDate + ' - ' + listoutDate}
              />
            </div>
            <p>
              {listNewsPromoDescription}
            </p>
          </div>
        </div>
      );
    }
  }

  render() {
    let { isLoading } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card plain>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable() }
            </CardBody>
          </Card>
        </GridItem>
      </Grid>
    );
  }
}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};