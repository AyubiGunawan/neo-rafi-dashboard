import { ACTIONS } from 'constants/index';

const initialState = {
  data: {},
  page: 1,
  count: 10
};

export default function reducer(state = initialState, action) {
  const { LIST_OF_USER_FETCHED } = ACTIONS;
  const { type, data, page, count } = action;

  switch (type) {
    case LIST_OF_USER_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        page,
        count,
        type
      };
    default:
      return state;
  }
}
