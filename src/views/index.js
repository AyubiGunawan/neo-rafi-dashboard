import LoginPage from './Login';
import NewsListPage from './NewsList';
import NewsDetailPage from './NewsDetail';
import UserListPage from './UserList';
import RewardListPage from './RewardList';
import RedeemListPage from './RedeemList';
import FrameListPage from './FrameList';
import PromoListPage from './PromoList';
import PromoDetailPage from './PromoDetail';
import UserDetailPage from './UserDetail';

const views = {
    LoginPage,
    NewsListPage,
    NewsDetailPage,
    UserListPage,
    RewardListPage,
    RedeemListPage,
    FrameListPage,
    PromoListPage,
    PromoDetailPage,
    UserDetailPage,
};

export default views;