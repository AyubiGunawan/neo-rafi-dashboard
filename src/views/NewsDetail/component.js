import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { ACTIONS } from 'constants/index';
import Quote from "components/Typography/Quote.jsx";

export default class Component extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      render: false
    }
  }

  componentDidMount() {
    const { actions, match } = this.props;
    actions.fenchGetDetailNews(match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    let { DETAIL_OF_NEWS_FETCHED } = ACTIONS;
    let { type } = nextProps;

    if (type === DETAIL_OF_NEWS_FETCHED) {
      this.setState({render: true});
    }
  }

  _renderTable() {

    if (!this.state.render) {
      return ( <h1> TIDAK ADA DATA </h1>
      );
    } else {
      let { classes, data } = this.props
      let { newsImage, newsCategory, newsDescription, newsStartDate, newsLocation, newsTitle } = data;
      return (
        <div>
          <div>
            <h2>{newsTitle}</h2>
            <img src={newsImage} alt='aww' width="100%" />
          </div>
          <div className={classes.typo}>
            <div className={classes.note}>
              <Quote
                text={newsCategory + ' - ' + newsLocation + '.'}
                author={' ' + newsStartDate}
              />
            </div>
            <p>
              {newsDescription}
            </p>
          </div>
        </div>
      );
    }
  }
  render() {
    let { isLoading } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card plain>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable() }
            </CardBody>
          </Card>
        </GridItem>
      </Grid>
    );
  }
}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};